module.exports = {
  presets: [
      'babel-preset-typescript-vue3',
      '@babel/preset-typescript',
      '@vue/cli-plugin-babel/preset',
  ]
}
